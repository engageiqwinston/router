<?php

namespace App\Http\Controllers;

use App\Entities\Notes;
use Illuminate\Http\Request;

class NotesController extends Controller
{

    public function keywords()
    {
        $data = Notes::select("title as name")->get();
        return response()->json($data);
    }
    
    public function search(Request $request)
    {
        $notes = Notes::where("title","LIKE","%{$request->keyword}%")->orderBy('created_at','DESC')->get();
        return response(['message' => 'success', 'code' => 200, 'lists' => $notes ]);
    }

    public function lists()
    {
        $notes = Notes::orderBy('created_at','DESC')->limit(5)->get();
        return response(['message' => 'success', 'code' => 200, 'lists' => $notes ]);
    }
    
    
    public function store(Request $request)
    {
        $request['user_id'] = \Auth::user()->id;
        $request->description =   htmlspecialchars($request->description);
        $notes = Notes::create(request()->all());
        return response(['message' => 'success', 'code' => 200, 'data' => $notes ]);
    }

    public function delete($id)
    {
        $notes = Notes::find($id);
        $notes->delete();
        return response(['message' => 'success', 'code' => 200 ]);
    }

    public function update(Request $request)
    {
        $note = Notes::find($request->id);
        $note->category_id = $request->category_id;
        $note->title = $request->title;
        $note->contents = $request->contents;

        $note->update();

        return response(['message' => 'success', 'data' => $note, 'code' => 200 ]);
    }
}
