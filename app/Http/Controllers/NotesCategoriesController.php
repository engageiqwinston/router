<?php

namespace App\Http\Controllers;

use App\Entities\NotesCategories;
use Illuminate\Http\Request;

class NotesCategoriesController extends Controller
{
    public function lists()
    {
        return response(['lists' => NotesCategories::get() , 'code' => 200 ]);
    }
}
