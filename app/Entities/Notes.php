<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $table = 'notes';

    protected $fillable = ['user_id','category_id', 'title','contents'];
    
    public function categories()
    {
        return $this->hasOne(NotesCategories::class,'id','category_id')->select(['id','name']);
    }

}
