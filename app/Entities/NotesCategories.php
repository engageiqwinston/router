<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class NotesCategories extends Model
{
    protected $table = 'notes_categories';
}
