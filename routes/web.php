<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@login');

Route::group(['middleware' => ['web'], 'prefix' => 'profile' ], function () {
    Auth::routes();
    Route::get('/{vue?}', 'HomeController@index')->name('home');
    Route::get('/notes/categories/get','NotesCategoriesController@lists');

    Route::get('/notes/get','NotesController@lists');
    Route::post('/notes/store','NotesController@store');
    Route::delete('/notes/delete/{id}','NotesController@delete');
    Route::get('/notes/keywords','NotesController@keywords');
    Route::get('/notes/search','NotesController@search');
    Route::patch('/notes/update','NotesController@update');


});


