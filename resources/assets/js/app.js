
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./typehead');

window.Vue = require('vue');
import VueRouter from 'vue-router';

import Vuex from 'vuex';


window.Vue.use(VueRouter);
window.Vue.use(Vuex);
var bus = new Vue();

window.Vue.$bus = bus;
Object.defineProperty(Vue.prototype, '$bus' , {
    get()
    {
        return bus;
    }
});


const store = new Vuex.Store({
    strict: true,
    state: {
        categories: [],
        notes : [],
        typeAhead : []
    },
    mutations: {
        setCategories (state,data) {
            state.categories = data;
        },
        addCategories(){

        },
        deleteCategories(){

        },

        setTypeAhead(state,data){
            state.typeAhead = data;
        },

        setNotes(state,notes){
            state.notes = notes;
        },
        
        addNote(state,note){
            state.notes.unshift(note);
        },
        
        updateNote(state,data){
            state.notes[data.index] = data.note;
        },
        
        deleteNote(state,index){
            state.notes.splice(index,1);
        }
    }
});


const router = new VueRouter({
    mode : 'history',
    routes  :
        [
            { 
                path: '/profile/home',
                component: require('./components/Home.vue') 
            },
            {
                path: '/profile/about',
                component: require('./components/About.vue')
            },
            { 
                path: '/profile/contact', 
                component: require('./components/Contact.vue') 
            },
            { 
                path: '/profile/notes', 
                component: require('./components/Notes.vue')
            }
        ]
});


const app = new Vue({
    store,
    router,
    el: '#app',
    mounted()
    {
        axios.get('/profile/notes/categories/get')
                .then(function(response) {
                    store.commit('setCategories',response.data.lists)
                }.bind(this));

        axios.get('/profile/notes/get')
            .then(function(response) {
                store.commit('setNotes',response.data.lists)
            }.bind(this));

        axios.get('/profile/notes/keywords')
            .then(function(response) {
                store.commit('setTypeAhead',response.data);
        }.bind(this));
    }
});

